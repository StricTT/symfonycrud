<?php
declare(strict_types=1);

namespace App\Service;

use App\Entity\Data;
use Swift_Mailer;
use Swift_Message;
use Twig_Environment;

class Mailer
{
    public const FROM_ADDRESS = 'posos2281488@gmail.com';

    /**
    * @var Swift_Mailer
     */
    private $mailer;

    /**
     * @var Twig_Environment
     */
    private $twig;


    public function __construct(
        Swift_Mailer $mailer,
        Twig_Environment  $twig
    )
    {
        $this->mailer = $mailer;
        $this->twig = $twig;
    }

    public function sendMessage(Data $data)
    {
        $mail = $this->twig->render('confirm.html.twig', ['data' => $data]);

        $message = new Swift_Message();

        $message
            ->setSubject('You have successfully registered')
            ->setFrom(self::FROM_ADDRESS)
            ->setTo($data->getEmail())
            ->setBody($mail, 'text/html');

        $this->mailer->send($message);
    }
}