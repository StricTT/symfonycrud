<?php


namespace App\Controller;


use App\Entity\Users;
use App\Form\FormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UsersController extends AbstractController
{
    /**
     * @Route("/index", name="users_index")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(Users::class)->findAll();

        return $this->render('index.html.twig', array('users' => $users));
    }

    /**
     * @Route("/new", name="users_new")
     */
    public function newAction(Request $request)
    {
        $users = new Users();
        $form = $this->createForm(FormType::class, $users);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $users = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($users);
            $em->flush();

            return $this->redirectToRoute('users_index');
        }
        return $this->render('new.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/show/{id}", name="users_show")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $users = $em->getRepository(Users::class)->find($id);


          return $this->render('show.html.twig', array('users' => $users));
    }

    /**
     * @Route("/edit/{id}", name="users_edit")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     */
    public function updateAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $id = $request->get('id');
        $users = $em->getRepository(Users::class)->find($id);

        if (!$users) {
            throw $this->createNotFoundException('No WTF id '.$id);
        }

        $editForm = $this->createForm(FormType::class, $users);
        $editForm->handleRequest($request);

        if($editForm->isSubmitted() && $editForm->isValid()){
            $em->persist($users);
            $em->flush();

            return $this->redirectToRoute('users_edit', array('id' => $id));
        }

        return $this->render('edit.html.twig', array('users' => $users,
            'edit_form' => $editForm->createView()));
    }

    /**
     * @Route("/delete/{id}", name="users_delete")
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function deleteAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $article = $em->getRepository(Users::class)->find($id);
        $em->remove($article);
        $em->flush();

        return $this->redirectToRoute('users_index');
    }
}

