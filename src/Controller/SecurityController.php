<?php


namespace App\Controller;


use App\Entity\Data;
use App\Form\RegisterFormType;
use App\Repository\UsersRepository;
use App\Service\CodeGenerator;
use App\Service\Mailer;
use http\Client\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class SecurityController extends Controller
{

    /**
    * @Route("/register", name="user_registration")
    */
    public function newAction(Request $request, UserPasswordEncoderInterface $passwordEncoder, Mailer $mailer, CodeGenerator $codeGenerator)
    {
        $users = new Data();
        $form = $this->createForm(RegisterFormType::class, $users);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $password = $passwordEncoder->encodePassword($users, $users->getPassword());
            $users->setPassword($password);
            $users->setConfirmationCode($codeGenerator->getConfirmationCode());

            $users = $form->getData();
            $em = $this->getDoctrine()->getManager();
            $em->persist($users);
            $em->flush();
            $mailer->sendMessage($users);

            $mailer->sendMessage($users);

            return $this->redirectToRoute('login');
        }

        return $this->render('register.html.twig', [
            'form' => $form->createView()
        ]);


    }

    /**
     * @Route("/confirm/{code}", name="confirm")
     */
    public function confirmEmail(UsersRepository $usersRepository, string $code)
    {

        $users = $usersRepository->findOneBy(['confirmationCode' => $code]);

        if ($users === null) {
            return new Response('404');
        }

        $users->setConfirmationCode('');

        $em = $this->getDoctrine()->getManager();

        $em->flush();

        return $this->render('passuracc.html.twig', [
            'users' => $users,
        ]);

    }







    /**
    * @Route("/login", name="login")
    */
    public function login (Request $request, AuthenticationUtils $utils)
    {
        $error = $utils->getLastAuthenticationError();
        $lastUsername = $utils->getLastUsername();

        return $this->render('login.html.twig', [
            'error'         => $error,
            'last_username' => $lastUsername
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public function logout()
    {

    }
}