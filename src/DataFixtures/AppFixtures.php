<?php


namespace App\DataFixtures;

use App\Entity\Users;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0; $i < 50; $i++) {
            $users = new Users();
            $users->setName('Alex');
            $users->setSurname('StricT');
            $users->setAge(mt_rand(1, 100));
            $manager->persist($users);
        }
        $manager->flush();
    }
}

